from distutils.core import setup

setup(
    name='Table counter',
    version='0.1',
    description='Django app for counting table values on template level',
    author='Jiri Dubansky',
    author_email='jiri@dubansky.cz',
    packages=[
        'django_table_counter',
        'django_table_counter.templatetags'
    ],
)

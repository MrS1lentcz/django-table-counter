from . import aggregators


TABLE_COUNTER_STORAGE = '_DEFAULT_TABLE_COUNTER'
TABLE_COUNTER_AGGREGATORS = {
    'sum': aggregators.tc_sum,
    'avg': aggregators.tc_avg,
}

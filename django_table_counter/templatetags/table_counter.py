from django import template

from django_table_counter.counter import Counter

register = template.Library()


@register.simple_tag(takes_context=True)
def counter_add(context, storage=None, key=None, value=None):
    c = Counter(context, extra_storage=storage)
    c.add(key=key, value=value)
    return ''


@register.simple_tag(takes_context=True)
def counter_sum(context, storage=None, key=None, default=None):
    c = Counter(context, extra_storage=storage)
    return c.aggregate('sum', key, default=default)


@register.simple_tag(takes_context=True)
def counter_avg(context, storage=None, key=None, default=None):
    c = Counter(context, extra_storage=storage)
    return c.aggregate('avg', key, default=default)

from . import settings


class Counter:
    def __init__(self, context, extra_storage=None):
        self.context = context
        self.data = extra_storage if extra_storage else context.get(settings.TABLE_COUNTER_STORAGE)

    def add(self, key, value):
        if value is not None:
            if key in self.data:
                self.data[key]['count'] += 1
                self.data[key]['values_list'].append(value)
            else:
                self.data[key] = {
                    'count': 1,
                    'values_list': [value]
                }

    def aggregate(self, name, key, default):
        func = settings.TABLE_COUNTER_AGGREGATORS[name]
        if key in self.data:
            return func(self.data[key], default)

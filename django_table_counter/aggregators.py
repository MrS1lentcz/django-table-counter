

def tc_sum(data, default):
    if data['count']:
        return sum(data['values_list'])
    return default


def tc_avg(data, default):
    if data['count']:
        return sum(data['values_list']) / data['count']
    return default

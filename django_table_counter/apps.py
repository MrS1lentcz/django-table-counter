from django.apps import AppConfig
from django.conf import settings
from . import settings as table_settings


class TableCounterConfig(AppConfig):
    name = 'django_table_counter'
    verbose_name = 'Table counter'

    def ready(self):
        settings.TABLE_COUNTER_STORAGE = getattr(settings, 'TABLE_COUNTER_STORAGE', table_settings.TABLE_COUNTER_STORAGE)
        settings.TABLE_COUNTER_AGGREGATORS = getattr(settings, 'TABLE_COUNTER_AGGREGATORS', table_settings.TABLE_COUNTER_AGGREGATORS)
